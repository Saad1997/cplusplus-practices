#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/saad/catkin_ws/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/saad/catkin_ws/devel/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/saad/catkin_ws/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD="/home/saad/catkin_ws/build"
export PYTHONPATH="/home/saad/catkin_ws/devel/lib/python2.7/dist-packages:/opt/ros/melodic/lib/python2.7/dist-packages:/usr/lib/python2.7.15+"
export ROS_PACKAGE_PATH="/home/saad/catkin_ws/src:/home/saad/catkin_ws/src/beginner_tutorials:/home/saad/catkin_ws/location_monitor:$ROS_PACKAGE_PATH"