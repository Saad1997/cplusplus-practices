# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "${prefix}/include".split(';') if "${prefix}/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;sensor_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lros_package_template_core".split(';') if "-lros_package_template_core" != "" else []
PROJECT_NAME = "ros_package_template"
PROJECT_SPACE_DIR = "/home/saad/catkin_ws/install"
PROJECT_VERSION = "0.1.0"
