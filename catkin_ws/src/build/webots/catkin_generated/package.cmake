set(_CATKIN_CURRENT_PACKAGE "webots")
set(webots_VERSION "2.0.6")
set(webots_MAINTAINER "Cyberbotics <support@cyberbotics.com>")
set(webots_PACKAGE_FORMAT "1")
set(webots_BUILD_DEPENDS "rospy" "roscpp" "std_msgs" "sensor_msgs" "message_generation" "tf")
set(webots_BUILD_EXPORT_DEPENDS "rospy" "roscpp" "std_msgs" "sensor_msgs" "message_runtime" "tf")
set(webots_BUILDTOOL_DEPENDS "catkin")
set(webots_BUILDTOOL_EXPORT_DEPENDS )
set(webots_EXEC_DEPENDS "rospy" "roscpp" "std_msgs" "sensor_msgs" "message_runtime" "tf")
set(webots_RUN_DEPENDS "rospy" "roscpp" "std_msgs" "sensor_msgs" "message_runtime" "tf")
set(webots_TEST_DEPENDS )
set(webots_DOC_DEPENDS )
set(webots_URL_WEBSITE "http://wiki.ros.org/webots_ros")
set(webots_URL_BUGTRACKER "")
set(webots_URL_REPOSITORY "")
set(webots_DEPRECATED "")