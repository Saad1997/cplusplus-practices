
"use strict";

let InitCircularTrajectory = require('./InitCircularTrajectory.js')
let GoToIncremental = require('./GoToIncremental.js')
let SetPIDParams = require('./SetPIDParams.js')
let GoTo = require('./GoTo.js')
let ClearWaypoints = require('./ClearWaypoints.js')
let InitRectTrajectory = require('./InitRectTrajectory.js')
let GetPIDParams = require('./GetPIDParams.js')
let SetSMControllerParams = require('./SetSMControllerParams.js')
let InitWaypointsFromFile = require('./InitWaypointsFromFile.js')
let StartTrajectory = require('./StartTrajectory.js')
let SwitchToAutomatic = require('./SwitchToAutomatic.js')
let InitWaypointSet = require('./InitWaypointSet.js')
let ResetController = require('./ResetController.js')
let SetMBSMControllerParams = require('./SetMBSMControllerParams.js')
let GetSMControllerParams = require('./GetSMControllerParams.js')
let IsRunningTrajectory = require('./IsRunningTrajectory.js')
let GetWaypoints = require('./GetWaypoints.js')
let Hold = require('./Hold.js')
let InitHelicalTrajectory = require('./InitHelicalTrajectory.js')
let GetMBSMControllerParams = require('./GetMBSMControllerParams.js')
let AddWaypoint = require('./AddWaypoint.js')
let SwitchToManual = require('./SwitchToManual.js')

module.exports = {
  InitCircularTrajectory: InitCircularTrajectory,
  GoToIncremental: GoToIncremental,
  SetPIDParams: SetPIDParams,
  GoTo: GoTo,
  ClearWaypoints: ClearWaypoints,
  InitRectTrajectory: InitRectTrajectory,
  GetPIDParams: GetPIDParams,
  SetSMControllerParams: SetSMControllerParams,
  InitWaypointsFromFile: InitWaypointsFromFile,
  StartTrajectory: StartTrajectory,
  SwitchToAutomatic: SwitchToAutomatic,
  InitWaypointSet: InitWaypointSet,
  ResetController: ResetController,
  SetMBSMControllerParams: SetMBSMControllerParams,
  GetSMControllerParams: GetSMControllerParams,
  IsRunningTrajectory: IsRunningTrajectory,
  GetWaypoints: GetWaypoints,
  Hold: Hold,
  InitHelicalTrajectory: InitHelicalTrajectory,
  GetMBSMControllerParams: GetMBSMControllerParams,
  AddWaypoint: AddWaypoint,
  SwitchToManual: SwitchToManual,
};
