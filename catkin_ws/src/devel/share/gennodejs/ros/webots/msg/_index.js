
"use strict";

let RecognitionObject = require('./RecognitionObject.js');
let BoolStamped = require('./BoolStamped.js');
let Int32Stamped = require('./Int32Stamped.js');
let Int8Stamped = require('./Int8Stamped.js');
let Float64Stamped = require('./Float64Stamped.js');
let RadarTarget = require('./RadarTarget.js');
let StringStamped = require('./StringStamped.js');

module.exports = {
  RecognitionObject: RecognitionObject,
  BoolStamped: BoolStamped,
  Int32Stamped: Int32Stamped,
  Int8Stamped: Int8Stamped,
  Float64Stamped: Float64Stamped,
  RadarTarget: RadarTarget,
  StringStamped: StringStamped,
};
