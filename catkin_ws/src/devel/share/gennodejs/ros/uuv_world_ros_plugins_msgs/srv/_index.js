
"use strict";

let SetCurrentDirection = require('./SetCurrentDirection.js')
let SetOriginSphericalCoord = require('./SetOriginSphericalCoord.js')
let TransformToSphericalCoord = require('./TransformToSphericalCoord.js')
let SetCurrentVelocity = require('./SetCurrentVelocity.js')
let TransformFromSphericalCoord = require('./TransformFromSphericalCoord.js')
let SetCurrentModel = require('./SetCurrentModel.js')
let GetCurrentModel = require('./GetCurrentModel.js')
let GetOriginSphericalCoord = require('./GetOriginSphericalCoord.js')

module.exports = {
  SetCurrentDirection: SetCurrentDirection,
  SetOriginSphericalCoord: SetOriginSphericalCoord,
  TransformToSphericalCoord: TransformToSphericalCoord,
  SetCurrentVelocity: SetCurrentVelocity,
  TransformFromSphericalCoord: TransformFromSphericalCoord,
  SetCurrentModel: SetCurrentModel,
  GetCurrentModel: GetCurrentModel,
  GetOriginSphericalCoord: GetOriginSphericalCoord,
};
