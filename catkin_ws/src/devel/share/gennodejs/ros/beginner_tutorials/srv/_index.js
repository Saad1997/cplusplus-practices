
"use strict";

let num = require('./num.js')
let AddTwoInts = require('./AddTwoInts.js')

module.exports = {
  num: num,
  AddTwoInts: AddTwoInts,
};
