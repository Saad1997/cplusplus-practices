;; Auto-generated. Do not edit!


(when (boundp 'webots::get_string)
  (if (not (find-package "WEBOTS"))
    (make-package "WEBOTS"))
  (shadow 'get_string (find-package "WEBOTS")))
(unless (find-package "WEBOTS::GET_STRING")
  (make-package "WEBOTS::GET_STRING"))
(unless (find-package "WEBOTS::GET_STRINGREQUEST")
  (make-package "WEBOTS::GET_STRINGREQUEST"))
(unless (find-package "WEBOTS::GET_STRINGRESPONSE")
  (make-package "WEBOTS::GET_STRINGRESPONSE"))

(in-package "ROS")





(defclass webots::get_stringRequest
  :super ros::object
  :slots (_ask ))

(defmethod webots::get_stringRequest
  (:init
   (&key
    ((:ask __ask) nil)
    )
   (send-super :init)
   (setq _ask __ask)
   self)
  (:ask
   (&optional __ask)
   (if __ask (setq _ask __ask)) _ask)
  (:serialization-length
   ()
   (+
    ;; bool _ask
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _ask
       (if _ask (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _ask
     (setq _ask (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass webots::get_stringResponse
  :super ros::object
  :slots (_value ))

(defmethod webots::get_stringResponse
  (:init
   (&key
    ((:value __value) "")
    )
   (send-super :init)
   (setq _value (string __value))
   self)
  (:value
   (&optional __value)
   (if __value (setq _value __value)) _value)
  (:serialization-length
   ()
   (+
    ;; string _value
    4 (length _value)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _value
       (write-long (length _value) s) (princ _value s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _value
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _value (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(defclass webots::get_string
  :super ros::object
  :slots ())

(setf (get webots::get_string :md5sum-) "3bf99d9257a34f6cdd01cd192a62b3df")
(setf (get webots::get_string :datatype-) "webots/get_string")
(setf (get webots::get_string :request) webots::get_stringRequest)
(setf (get webots::get_string :response) webots::get_stringResponse)

(defmethod webots::get_stringRequest
  (:response () (instance webots::get_stringResponse :init)))

(setf (get webots::get_stringRequest :md5sum-) "3bf99d9257a34f6cdd01cd192a62b3df")
(setf (get webots::get_stringRequest :datatype-) "webots/get_stringRequest")
(setf (get webots::get_stringRequest :definition-)
      "bool ask
---
string value

")

(setf (get webots::get_stringResponse :md5sum-) "3bf99d9257a34f6cdd01cd192a62b3df")
(setf (get webots::get_stringResponse :datatype-) "webots/get_stringResponse")
(setf (get webots::get_stringResponse :definition-)
      "bool ask
---
string value

")



(provide :webots/get_string "3bf99d9257a34f6cdd01cd192a62b3df")


