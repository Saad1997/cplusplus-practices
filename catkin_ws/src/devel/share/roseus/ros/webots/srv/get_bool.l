;; Auto-generated. Do not edit!


(when (boundp 'webots::get_bool)
  (if (not (find-package "WEBOTS"))
    (make-package "WEBOTS"))
  (shadow 'get_bool (find-package "WEBOTS")))
(unless (find-package "WEBOTS::GET_BOOL")
  (make-package "WEBOTS::GET_BOOL"))
(unless (find-package "WEBOTS::GET_BOOLREQUEST")
  (make-package "WEBOTS::GET_BOOLREQUEST"))
(unless (find-package "WEBOTS::GET_BOOLRESPONSE")
  (make-package "WEBOTS::GET_BOOLRESPONSE"))

(in-package "ROS")





(defclass webots::get_boolRequest
  :super ros::object
  :slots (_ask ))

(defmethod webots::get_boolRequest
  (:init
   (&key
    ((:ask __ask) nil)
    )
   (send-super :init)
   (setq _ask __ask)
   self)
  (:ask
   (&optional __ask)
   (if __ask (setq _ask __ask)) _ask)
  (:serialization-length
   ()
   (+
    ;; bool _ask
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _ask
       (if _ask (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _ask
     (setq _ask (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass webots::get_boolResponse
  :super ros::object
  :slots (_value ))

(defmethod webots::get_boolResponse
  (:init
   (&key
    ((:value __value) nil)
    )
   (send-super :init)
   (setq _value __value)
   self)
  (:value
   (&optional __value)
   (if __value (setq _value __value)) _value)
  (:serialization-length
   ()
   (+
    ;; bool _value
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _value
       (if _value (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _value
     (setq _value (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass webots::get_bool
  :super ros::object
  :slots ())

(setf (get webots::get_bool :md5sum-) "155bbaa7eff4769d41b3327e2ec91080")
(setf (get webots::get_bool :datatype-) "webots/get_bool")
(setf (get webots::get_bool :request) webots::get_boolRequest)
(setf (get webots::get_bool :response) webots::get_boolResponse)

(defmethod webots::get_boolRequest
  (:response () (instance webots::get_boolResponse :init)))

(setf (get webots::get_boolRequest :md5sum-) "155bbaa7eff4769d41b3327e2ec91080")
(setf (get webots::get_boolRequest :datatype-) "webots/get_boolRequest")
(setf (get webots::get_boolRequest :definition-)
      "bool ask
---
bool value

")

(setf (get webots::get_boolResponse :md5sum-) "155bbaa7eff4769d41b3327e2ec91080")
(setf (get webots::get_boolResponse :datatype-) "webots/get_boolResponse")
(setf (get webots::get_boolResponse :definition-)
      "bool ask
---
bool value

")



(provide :webots/get_bool "155bbaa7eff4769d41b3327e2ec91080")


