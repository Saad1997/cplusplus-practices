;; Auto-generated. Do not edit!


(when (boundp 'webots::set_string)
  (if (not (find-package "WEBOTS"))
    (make-package "WEBOTS"))
  (shadow 'set_string (find-package "WEBOTS")))
(unless (find-package "WEBOTS::SET_STRING")
  (make-package "WEBOTS::SET_STRING"))
(unless (find-package "WEBOTS::SET_STRINGREQUEST")
  (make-package "WEBOTS::SET_STRINGREQUEST"))
(unless (find-package "WEBOTS::SET_STRINGRESPONSE")
  (make-package "WEBOTS::SET_STRINGRESPONSE"))

(in-package "ROS")





(defclass webots::set_stringRequest
  :super ros::object
  :slots (_value ))

(defmethod webots::set_stringRequest
  (:init
   (&key
    ((:value __value) "")
    )
   (send-super :init)
   (setq _value (string __value))
   self)
  (:value
   (&optional __value)
   (if __value (setq _value __value)) _value)
  (:serialization-length
   ()
   (+
    ;; string _value
    4 (length _value)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _value
       (write-long (length _value) s) (princ _value s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _value
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _value (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(defclass webots::set_stringResponse
  :super ros::object
  :slots (_success ))

(defmethod webots::set_stringResponse
  (:init
   (&key
    ((:success __success) nil)
    )
   (send-super :init)
   (setq _success __success)
   self)
  (:success
   (&optional __success)
   (if __success (setq _success __success)) _success)
  (:serialization-length
   ()
   (+
    ;; bool _success
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _success
       (if _success (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _success
     (setq _success (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass webots::set_string
  :super ros::object
  :slots ())

(setf (get webots::set_string :md5sum-) "0462bc0e878964615c49ad8ef45df667")
(setf (get webots::set_string :datatype-) "webots/set_string")
(setf (get webots::set_string :request) webots::set_stringRequest)
(setf (get webots::set_string :response) webots::set_stringResponse)

(defmethod webots::set_stringRequest
  (:response () (instance webots::set_stringResponse :init)))

(setf (get webots::set_stringRequest :md5sum-) "0462bc0e878964615c49ad8ef45df667")
(setf (get webots::set_stringRequest :datatype-) "webots/set_stringRequest")
(setf (get webots::set_stringRequest :definition-)
      "string value
---
bool success

")

(setf (get webots::set_stringResponse :md5sum-) "0462bc0e878964615c49ad8ef45df667")
(setf (get webots::set_stringResponse :datatype-) "webots/set_stringResponse")
(setf (get webots::set_stringResponse :definition-)
      "string value
---
bool success

")



(provide :webots/set_string "0462bc0e878964615c49ad8ef45df667")


