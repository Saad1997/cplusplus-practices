(cl:defpackage beginner_tutorials-srv
  (:use )
  (:export
   "ADDTWOINTS"
   "<ADDTWOINTS-REQUEST>"
   "ADDTWOINTS-REQUEST"
   "<ADDTWOINTS-RESPONSE>"
   "ADDTWOINTS-RESPONSE"
   "NUM"
   "<NUM-REQUEST>"
   "NUM-REQUEST"
   "<NUM-RESPONSE>"
   "NUM-RESPONSE"
  ))

