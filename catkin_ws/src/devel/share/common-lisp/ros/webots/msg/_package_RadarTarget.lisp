(cl:in-package webots-msg)
(cl:export '(HEADER-VAL
          HEADER
          DISTANCE-VAL
          DISTANCE
          RECEIVEDPOWER-VAL
          RECEIVEDPOWER
          SPEED-VAL
          SPEED
          AZIMUTH-VAL
          AZIMUTH
))