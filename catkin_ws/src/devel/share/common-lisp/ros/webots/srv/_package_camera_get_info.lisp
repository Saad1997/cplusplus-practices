(cl:in-package webots-srv)
(cl:export '(ASK-VAL
          ASK
          WIDTH-VAL
          WIDTH
          HEIGHT-VAL
          HEIGHT
          FOV-VAL
          FOV
          NEARRANGE-VAL
          NEARRANGE
))