(cl:in-package webots-srv)
(cl:export '(ASK-VAL
          ASK
          FOCALLENGTH-VAL
          FOCALLENGTH
          FOCALDISTANCE-VAL
          FOCALDISTANCE
          MAXFOCALDISTANCE-VAL
          MAXFOCALDISTANCE
          MINFOCALDISTANCE-VAL
          MINFOCALDISTANCE
))