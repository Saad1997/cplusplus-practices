(cl:in-package webots-srv)
(cl:export '(FONT-VAL
          FONT
          SIZE-VAL
          SIZE
          ANTIALIASING-VAL
          ANTIALIASING
          SUCCESS-VAL
          SUCCESS
))