(cl:in-package webots-srv)
(cl:export '(ASK-VAL
          ASK
          FREQUENCY-VAL
          FREQUENCY
          MINFREQUENCY-VAL
          MINFREQUENCY
          MAXFREQUENCY-VAL
          MAXFREQUENCY
))