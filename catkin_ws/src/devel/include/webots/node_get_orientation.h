// Generated by gencpp from file webots/node_get_orientation.msg
// DO NOT EDIT!


#ifndef WEBOTS_MESSAGE_NODE_GET_ORIENTATION_H
#define WEBOTS_MESSAGE_NODE_GET_ORIENTATION_H

#include <ros/service_traits.h>


#include <webots/node_get_orientationRequest.h>
#include <webots/node_get_orientationResponse.h>


namespace webots
{

struct node_get_orientation
{

typedef node_get_orientationRequest Request;
typedef node_get_orientationResponse Response;
Request request;
Response response;

typedef Request RequestType;
typedef Response ResponseType;

}; // struct node_get_orientation
} // namespace webots


namespace ros
{
namespace service_traits
{


template<>
struct MD5Sum< ::webots::node_get_orientation > {
  static const char* value()
  {
    return "0ee7131d7182bae6debc79ddfcc90ddd";
  }

  static const char* value(const ::webots::node_get_orientation&) { return value(); }
};

template<>
struct DataType< ::webots::node_get_orientation > {
  static const char* value()
  {
    return "webots/node_get_orientation";
  }

  static const char* value(const ::webots::node_get_orientation&) { return value(); }
};


// service_traits::MD5Sum< ::webots::node_get_orientationRequest> should match
// service_traits::MD5Sum< ::webots::node_get_orientation >
template<>
struct MD5Sum< ::webots::node_get_orientationRequest>
{
  static const char* value()
  {
    return MD5Sum< ::webots::node_get_orientation >::value();
  }
  static const char* value(const ::webots::node_get_orientationRequest&)
  {
    return value();
  }
};

// service_traits::DataType< ::webots::node_get_orientationRequest> should match
// service_traits::DataType< ::webots::node_get_orientation >
template<>
struct DataType< ::webots::node_get_orientationRequest>
{
  static const char* value()
  {
    return DataType< ::webots::node_get_orientation >::value();
  }
  static const char* value(const ::webots::node_get_orientationRequest&)
  {
    return value();
  }
};

// service_traits::MD5Sum< ::webots::node_get_orientationResponse> should match
// service_traits::MD5Sum< ::webots::node_get_orientation >
template<>
struct MD5Sum< ::webots::node_get_orientationResponse>
{
  static const char* value()
  {
    return MD5Sum< ::webots::node_get_orientation >::value();
  }
  static const char* value(const ::webots::node_get_orientationResponse&)
  {
    return value();
  }
};

// service_traits::DataType< ::webots::node_get_orientationResponse> should match
// service_traits::DataType< ::webots::node_get_orientation >
template<>
struct DataType< ::webots::node_get_orientationResponse>
{
  static const char* value()
  {
    return DataType< ::webots::node_get_orientation >::value();
  }
  static const char* value(const ::webots::node_get_orientationResponse&)
  {
    return value();
  }
};

} // namespace service_traits
} // namespace ros

#endif // WEBOTS_MESSAGE_NODE_GET_ORIENTATION_H
