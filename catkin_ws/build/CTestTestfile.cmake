# CMake generated Testfile for 
# Source directory: /home/saad/catkin_ws/src
# Build directory: /home/saad/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("joint_state_publisher/joint_state_publisher_gui")
subdirs("joint_state_publisher/joint_state_publisher")
subdirs("pulsar")
