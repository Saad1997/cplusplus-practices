#!/usr/bin/env python
import rospy
import math
from nav_msgs.msg import Odometry
from  beginner_tutorials.msg import LandmarkDistance
landmarks=[]
landmarks.append(("cafe_table",0.019956,-1.520000))
landmarks.append(("Mailbox",0.040044,0.510000))
print landmarks
def distance(x1,y1,landmark_name,c_x,c_y):
        new_x=x1-c_x
        new_y=y1-c_y
	dist=math.sqrt((new_x*new_x)+(new_y*new_y))
        global closest_name
	global closest_distance
	
	if dist<=1:
               print  rospy.loginfo("{},x_new= {},y_new= {}".format(landmark_name,new_x,new_y))
               closest_name= landmark_name
	       closest_distance=dist
            
	return dist

class landmark_monitor(object):
	def __init__(self,pub):
		self.pub=pub
		
	#	ld=distance()
	#	self.pub.publish(distance)



	def callback(self,msg):

			x=msg.pose.pose.position.x
			y=msg.pose.pose.position.y
			rospy.loginfo("x:{},y:{}".format(x,y))
			
			for landmark_name,l_x,l_y in landmarks:
				distance(x,y,landmark_name,l_x,l_y)
				
			ld=LandmarkDistance()
			
	               	ld.name=closest_name
			ld.distance=closest_distance
			print(ld)
			self.pub.publish(ld)

##	ld=distance()	self.pub.publish(distance)

def listener():

	rospy.init_node('location', anonymous=True)
	pub = rospy.Publisher('LandmarkDistance', LandmarkDistance, queue_size=10)
	monitor= landmark_monitor(pub)
	rospy.Subscriber('/odom', Odometry,monitor.callback)

	# rospy.Subscriber('/landmark', Odometry, callback)
	rospy.spin()
if __name__ == '__main__':
    listener()

