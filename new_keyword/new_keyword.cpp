//new is operator

#include <iostream>
#include <string>

using String = std::string;
class Entity{
private:
    string n_Name;
public:
    Enity():m_name("Unknown"){}
    Entity(const String& name):m_Name(name){}
    const String& GetName() const(){ // first const will return const reference to String //2nd const say that this function can't change any member variable of class for example this->n_Name ="ali" is not allowed inside this function as it can't modify member variable of class
        return m_Name;
    };
};

int main(){
     int* b= new int;//created 4 bytes(one integer) of memory //integer pointer b pointing to integer heap memory // new keyword will call the c function malloc which will allocate on heap memory=data and return pointer to taht palock of memory
     // new keyword used to allocate memory on heap // heap is leftover memory after program is loaded and stack memory is allocated. // stack memory is like cache i cpu, it is faster than heap and used to store values of different registers, it is fast because it applies last in/first out design.
     //heap memory alwasy must be manually freed but it can be avoided by using a smart pointer class or simillar
    
     int* c = new int[50];//created 200 bytes of memory(50 integers, 1 integer = 4 bytes)
     Entity* e1 = new Entity; // new keyword also call constructor//also created heap memory and stored Entity in that memory
     Entity* e= new Entity[50]//created constructor and also here it will see how much data inside Enityt constructore is defined in calss, it will multiply the size of taht data in bytes with 50 and will create memory of then this size// more specifically it creaed 50 Entities in a row in heap memory
     
     //Behind the scne ewhat new keyword do is it calls malloc() function from c which created memory in c++ 
     Entity* e= (Entity*)malloc(sizeof(Entity));//this will create a memory of size of Entity and return a pointer to that memory but will not call constructor 
     delete e;//manually freeing heap memory// it will also delete destrcutor
     delete[] c; //because c was declared using array brackets
     Entity* e3= new(c) Entity; //placement new
      
    
    
}
