//Smart Pointers
//Unique Pointers: Scope pointer, delete when it goea out of scope
//you cant copy them, the memory thats pointing to after death of pointer got free
//

#include <memory> // you need to include it for using shared pointers

class Entity{
public:
    Entity(){
    
        std::cout<<<"Created Entity"<<std::endl;
        
    }
    ~Entity(){
        std::cout<<"destroyed Entity"<<std::endl;
    }
    
print() {}
};

int main(){
    
    std::unique_ptr<Entity> entity(new Entity()); //creating unique pointer object from class Entity
    //or
    std::unique_ptr<Entity> entity=std::make_unique<Entity>();//safest way to create pointer object using unique pointer
    entity->print();
}

//problem with unique pointer is you can't copy it
//example 
 std::unique_pte<Entity> e0 = entity; // can't do this, this will create an error becuase if one of unique ptr dies all of them dies and memory gets free
 
 //So for sharing we use shared pointers
 //shared ptr implementation is upto compiler
 //The way shred ptr works is by using reference counting, as soon as reference to the pointer goes to 0 it gets deleted
 
 
 //example shared pointers
 
 std::shared_ptr<Entity> sharedentity=std::make_shared<Entity>(); // it creates only one memory thats combination of both control and memory block for object 
 //or
 std::shred_ptr<Entity> sharedentity(new Entity());// not a preferrable way, it construtcs two blocks of memories, first one to create a memory using new and second one to create memory for control block to use refrences or cout for refrences to shared_ptr
 std::shared_ptr<Entity> e0= sharedentity;//no error because we can copy shared pointers
 
//the given example demonstrate that shred_ptr are not independent of scope
int main(){ 
            {   
    std::shared_ptr<Entity> e0;
    
    { // scope of shared_ptr starts from here
        std::shared_ptr<Entity> sharedentity= std::make_shared<Entity>(); // this will create the pointer object // pressing entre for first time after running code  will decay sharedentity
        e0= sharedentity; // but this will keep sharedenityt alive even after using it once so our object will not die yet 
    
       }// scope of shared_ptr dies here

  //2nd time our object will be called from this scope as shared_ptr e0 is defined here, now when we press entre our object will die because there is no more refrence to shared_ptr sharedentity after this scope 
                
            }   
    std::cin.get();//take input from user 
}
//when all refrences are gone thats when underlying entiity dies


//Weak Pointers 
int main(){
    
    {
        std::weak_ptr<Entity> e0;
        {
    
            std::shared_ptr<Entity> sharedentity= std::make_shared<Entity>(); // this will create the pointer object 
            e0= sharedentity; // assign shared_ptr to weak_ptr   
    
        }
//the entity will be destroyed as long as it get out of this scope
//weak pointer can be used to check if entity is still alive or not
    }
}


// examples smart pointer
void my_func(){//example of pointer using new and delte keyword
    int* valuePtr = new int(15);// valuePtr is a pointer indicating a memory address which has 15 in it!
    int x=45;
    if(x==45){
        return;// here we have a memory leak valueptr is not detected
    
        delete valuePtr;
    }
    
    
}

//same example using unique pointers
#include <memory>
void my_function{
    std::unique_ptr<int> valuePtr(new int(15));
    int x=45;
    
    if(x==45)
        return;// no memory leak
}//here this pointer is deleted as we moved out of scope of pointer
int main()
{
}

// unique pointer can also be assigned with pointer later on like this example
std::unique_ptr<int> valuePtr;
valuePTr.reset(new int(47));
//an object created or managed by unique pointers can be accessed the same way as it can be with raw pointers, For example
std::unique_ptr<std::string> strPtr(new std::string);
strPtr->assign("Hello");
//Unique_ptr<> supports move semantics where pointer is moved from one unique_ptr<> to another, which invalidates the first unique_ptr<>.
//For example

#include <memory>
int main(){
    std::unique_ptr<int> valuePtr(new int(15));
    std::unique_ptr<int> valuePtrNow(std::move(valuePtr));
    std::cout<<"valuePtrNow="<<*valuePtrNow<<std::endl;
    std::cout<<"Has valuePtr an associated object?"<<std::boolalpha<<static_cast<bool>(valuePtr)<<std::endl;

}
//results
//valuePtrNow = 15
//Has valuePtr an associated object? false//valuePtr doesn't exist any more

    //another exapmle of using unique pointers

#include <memory>

struct B{
    virtual void bar(){ std::cout<<"B::bar\n" }
    virtual ~B()= default;
    
};

struct D:B{
    D(){std::cout<<"D::D\n"}
    ~D(){std::cout<<"D::~D\n"}
};

std::unique_ptr<D> pass_therough(std::unique_ptr<D> p)
{
    p->bar();
    return p;
    
}

void close_file(std::FILE* fp){std::fclose(fp);}


int main(){
    std::cout<<"unique ownership semantic demo\n";
    {
        auto p= std::make_unique<D>();//p is aunique pointer that owns D
        auto q= pass_through(std::move(p));
        assert(!p);//now p owns nothing and holds a null pointer
        q->bar();// now q owns D objects
    }//~D called here
    std::cout<<"Runtime Polymorphism\n";
    {
        std::unique_ptr<B> p= std::make_unique<D>();
        p->bar();
        std::vector<std::unique_ptr<B>> v;//unique pointer can be stored in container
        v.push_back(std::make_uniqe<D>());//insertion of unique pointer object created from struct D 
        v.push_back(std::move(p));//insertion of unique pointer p
        v.emplace_back(new D);//insertion of pointer object created from struct D
        for (auto& p:v) 
            p->bar();//virtual dispatch
    }//~D called for three time
    }
}


//*_________________________________________________________________________*


//Shared Pointers Examples

std::shared_ptr<Entity> e= std::make_shared<Entity>();


