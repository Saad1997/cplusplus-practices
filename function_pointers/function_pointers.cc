//Syntax
/**
 * @brief funcptr is pointer to function returning integer
 **/
int(*funptr)(){} ;

/**
 * @brief this is not a right way to define function pointer. This represents a pointr pointing to integer returend by funcptr
 */
int *funcptr(){}

/**
 * @brief To make a constant function pointer 
 * @brief putting const before int will represent a function pointer pointing to const integer 
 */
int (*const funcptr)(){};
...............................................................................................................................
//Example

/**
 * @brief using dereference variable
 */
int foo(int a){
    return a=5;
}
int give(int a){
    return a=7;
}

int main(){

int (*funcptr)(){&foo  //pointing to function foo
    
}
    funcptr=&give; //funcptr now pointing to give
}

..................................................................................................................................

//Use to call function using explicit call and implicit call

/**
 * @brief calling a function using function pointer
 * @brief using explicit dereference
 **/

int foo(int a){
    return a;
}

int main(){
    int (*funcptr)(int){&foo}; //initializing funcptr pointing to address of function foo
    (*funcptr)(4);//calling foo using funcptr explicitally//will dereference foo and will call it
}

/**
 * @brief 2nd is via implicit dereferencing
 **/

int foo(int a){
    return a;
}

int main(){
    int (*funcptr)(int){&foo};
    funcptr(5); ;;simillar to calling function

    return 0;
}

............................................................................................................................
//Used to call callback function// function used inside other functions as arguments are called as call back functions

/**
 * @brief function pointers are used for passing functions as arguments to other functions 
 * @brief as an example a sorting algorithem will be developed
 **/

#include <utility>//used for std::swap
#include <iostream>
using namespace std;
/**
 * @brief arrange element of array in way specified by function_pointer *comparison_function, user will have choice to use              function of his own will for comparison
 * @param *array pointer to an array  
 * @param size size of array
 * @param *comparison_function function ptr to function used for coparison i.e. asscending or descending
 **/
void sorting_numbers(int *array, int size, bool (*comparison_function)(int,int))
{
    for(starting_index=0;starting_index<(size-1);++starting_index)
    {
        int best_index=starting_index;
        for(int current_index= starting_index+1; current_index<size; ++current_index){
            if(comparison_function(array[best_index],array[current_index])){
                best_index=current_index;
            }
        }
        std::swap(array[starting_index],array[best_index]);
    }
    
}

/**
 * @brief compare two elements
 * @param int integer
 * @param int integer
 * @return true if x greater than y
 * @return flase if x less than y
 **/
bool asscending(int x, int y){
    return x>y;
}

/**
 * @brief compare two elements
 * @param int integer
 * @param int integer
 * @return true if x less than y
 * @return flase if x greater than y
 **/
bool descending(int x, int y){
    return x<y;
}

/**
 * @brief print an array
 * @param *array array
 * @param size size of array
**/
 void printarray(int *array, int size){
    
    for(int i=0; i<size; i++){
        std::cout<<array[i]<<std::endl;
    }    
}


int main(){
    int array[9]{5,6,2,3,0,1,4,7,8};
    sorting_numbers(array,9,asscending);
    printarray(array,9);
    return 0;
}

