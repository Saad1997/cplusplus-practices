//Operator Overloading reffers to hanging behaviour of operator in your program

#include <iostream>
#include <string>
struct Vector2{//fields are always public in struct, that is the difference between struct and class
    float x, y;
    Vector2(float x, float y)
        :x(x),y(y){} //constructor
    Vector2 Add(const Vector2& other) const{ //add function which is gonna return brandnew Vector2 but it will not change any variable in struct Vector2 so we declare it with const at the end
    //we passed a reference to avoid copying
    return Vector2(x+other.x,y+other.y);
    }
    Vector2 Multiply(const Vector2& other) const{ //add function which is gonna return brandnew Vector2 but it will not change any variable in struct Vector2 so we declare it with const at the end
    //we passed a reference to avoid copying
    return Vector2(x*other.x,y*other.y);
    }
    //defining + operator
    Vector2 operator+(const Vector2& other) const{
        return Add(other);
    }
    
    Vector2 operator*(const Vector2& other) const{
        return Multiply(other);
    }
};

std::ostream& operator<<(std::ostream& stream, const Vector2& other){ //defining << operator to output struct instance //stream instance is for cout //and then we provided what we want to cout
    stream<< other.x << "," <<other.y;
    return stream;
}


int main()
{
    Vector2 position(4.0f,4.0f);
    Vector2 speed(0.5f, 1.5f);
    Vector2 powerup(2.0f,4.0f);
    Vector2 result= position.Add(speed);//adding speed and position
    Vector2 result1= position.Add(speed.Multiply(powerup));//multiply speed and powerup and then add result to position // a complex way to add and multiply struct and variables. This can be done by operator overloading For example 
    Vector2 result2= position+speed*powerup; // now we need to define these operators + and *
    
    std::cout<<result2<<std::endl; //using above defined << operator //if we dont define this operator we are not able to cout struct
    std::cin.get();
    return 0;
}
