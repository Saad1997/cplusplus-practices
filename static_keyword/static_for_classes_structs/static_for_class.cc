//Static fo Classes:
//When static variables are defined inside class, changing them in one instance changes them in all of instances.





#include <iostream>
struct Entity{ 
    //we haven't mentioned public here unlike classes because in structs every thing is already public unlike classes
    static int x,y;
    
    void Print()
    {
        std::cout<<x<<","<<y<<std::endl;
        
    }
    //creating a static method
    
    static void StaticPrint() //static metohd always take static variables as arguments, assignment of non static variable will not work for static methods
    {
        std::cout<<x<<","<<y<<std::endl;
    }
    
};


    //If we want to pass non static variables to static method we need to pass 
    // instance of class as argument to the static method
    //for example int x, y;
    /*
     static void Print(Entity e){
     std::cout<<e.x<<","<<e.y<<std::endl; // here now we are accessing non static variable using static method
     }
     */

int Entity::x;//whenever you defined static variable inside class you need to initialize them outside the class also
int Entity::y;// //


int main(){
   // Entity e; usually we define a class instance and then acess variables inside class but for static variable this is different, we don't have any need to create instance of class to access static variables 
    //e.x=2; will give error, not allowed for static variables
    //e.y=5;//
    //Correct way to access static variables inside class
    Entity e;
    Entity::x=2;
    Entity::y=3;
    
    //Entity e1;
    //e1.x=5;
    //e1.y=8;
    Entity::x=5;
    Entity::y=8;
    e.Print(); //this will give x and y equal to 5 and 8 because they are static variables defined inside class and changing their value  will change their values for all instances
    e.StaticPrint();

    
    
    return 0;
    
    
}

