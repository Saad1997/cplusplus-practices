//static keyword in local scope

#include <iostream>


//Step2
//check this after you have check static_local example step1
//check Step1 first and all code that belongs to step 1
class Singleton{//creating instance inside the class and using a function to access other function of class 
public:
   static Singleton& Get(){  
         static Singleton instance; //here if we will remove static keyword, the lifetime of instance will be destroyed as soon as code moves out of curly brackets of Get() , Pytting static keyword extends its life time to forever  
        return instance;
    }
    void hello(){
        std::cout<<"hey"<<std::endl;
    }
};


//Step1
void function(){
    static int i; //the scope of i is local to this function but it is simillar to defining i outside function and then using it in function. The only difference is if we will define i outside of function it will have global scope and thus we can change its value anywhere. But in case of this static definition of i inside function we can not chnage value of i outside function as it has scope local to function
    i++;
    std::cout<<i<<std::endl;
}


int main(){
    
    function();//Step 1
    // i=3; //prohibited because we can not change value of i as i has local scope to function only
    function();//Step 1
    function();//Step 1

    
    Singleton::Get().hello(); //Step2
}
