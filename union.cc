/**
 * @Brief union provides opportunity to write in to different bytes of memory location assigned to struct 
 * 
 */

union whatever{ 
    short what;//16 bits
    struct { 
             char lo;//lower 8 bits assigned to lo
             char hi;//8 msbs assigned to hi
    }idk;
}

int main(){
    whatever needobject;
    needobject.what=0xBEWD; //what equals to BEWD
    needobject.idk.hi= 0xFO; //what equals to FOWD    
}


/**
 * @Brief lets see one more example
 * other than Uint8 type all other types are structs having details of responses to the particular event  
 */

 union SDL_event{
  //different events 
  Uint8 type;
  SDL_ActiveEvent active;
  SDL_KeyboardEvent key;
  SDL_MouseMotionEvent motion;
  SDL_MouseButtonEvent button;
  SDL_JoyAxisEvent jaxis;
  SDL_JoyBallEvent jball;
  SDL_JoyHatEvent jhat;
  SDL_JoyButtonEvent jbutton;
  SDL_ResizeEvent resize;
  SDL_ExposeEvent expose;
  SDL_QuitEvent quit;
  SDL_UserEvent user;
  SDL_SysWMEvent syswm;
    
  struct SDL_KeyboardEvent{
      Uint8 type;
      Uint8 button;
      Uint8 state;
      Uint16 x,y;
}    
}

int main(){
    SDL_event create_obj;
    create_obj.button=1;
    create_obj.type=2;
    return 0;
    
}
