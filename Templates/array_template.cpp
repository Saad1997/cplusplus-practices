//Defining and printing array using templates
#include <array>
#include <iostream>
using namespace std;

template <class t_type, size_t t_dim>
    std::array<t_type, t_dim> create_default_array (t_type value)
    {
        std::array<t_type, t_dim> a;
        a.fill(value);
        return a;
    }
int main(){
   const std::array<double, 5> &Kp   = create_default_array<double, 5>(0);
   for (int i = 5 - 1; i >= 0; i--)
    cout << Kp[i];

}
