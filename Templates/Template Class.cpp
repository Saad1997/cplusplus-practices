//C++ practices
// We are going to create a class using template in this code

#include <iostream>
#include <string>
using namespace std;
template<typename T, int value>
class Array{
private:
  T array[value];
public:
  T Getsize() const{
    return value;
  }
};
int main(){
  Array<int,5> a;
  std::cout<< a.Getsize();
return 0;
}
