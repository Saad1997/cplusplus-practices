
&ref//gives memory address of ref

double *ptr; //double type pointer, now we need to assign it with a address of some double variable
double a;
ptr=&a;//assigned with address of a

*ptr;//gives whatever is placed inside a



int a=5;
int& ref=a;//& is part of type// int& means integer type reference// ref got valeue of a here
ref =2;//cahnging value of ref will cahnge value of a
cout<<a;

void increment(int* value)
{
    (*value)++;//incrementing value
}
int a=5;
increment(&a);


void increment(int& value)
{
    value++;
}
int a=5;
increment(a);



int a=5;
int b=9;
int& ref=a;// reference to a
ref=b;//not allowed

int* ref=&a;//we referenced to the address of a
*ref=2;//we set at address of a value of 2 so ref has value of 2.
ref= &b; //we set reference to address of b
*ref=5;//set value at address of b=5 which means now ref has value of 5.
