#include <iostream>
using namespace std;
inline void display(int a){ //when this funtion will be called in code , instead of calling it  compiler will replace function call with the statements inside funcition thus improving a performance a bit
    std::cout<<a<<std::endl;
}

int main(){
    
    display(1);
    return 0;
}
