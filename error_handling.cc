//EXCEPTIONS
double num1=0,num2=0;
try{
    if(num2==0){
        throw "Division by zero";
    }
    else{
    
        printf("%.1f/%.1f = %.2f", num1, num2, (num1/num2));
    }
}
catch(const char* exp){
    
    std::cout<<"Error:" << exp <<"\n";
}


//Other way to throw EXCEPTIONS

try{
    std::cout<<"Throwing exception\n";
    throw std::runtime_error("Error Occured");
    std::cout<<"Can you print me?\n"; // to check if this statement will run after runtime_error or not
}
    catch(std::exception &exp){
        std::cout<<"Handled exception:"<< exp.what()<<"\n";//calling what function will print the exception given in throw
}

// another example//use of pre defined exception in std::exception class

try{
    MyFunc(256);// this will throw an exception
}
catch(invalid_argument& e){//invalid invalid_argument is exception here
    std::cout<<e.what()<<endl;
    return -1;
}

//

//exception safety
//1-keeping classes simple and use of smart pointers
//example old version 
class NDResourceClass{
private:
    int* m_p;
    float* m_q;
public:
    NDRResourceClass():m_p(0),m_q(0){
        m_p= new int;
        m_q= new float;
    }
    
    ~NDResourceClass(){
        delete m_p;
        delete m_q;
    }

    // potential leak, when destructor will throw an exception destructorwill not be invoked
    
};

// to remove this potential leak we use shared pointers

class NDResourceClass{
    
private:
    shared_ptr<int> m_p;
    shared_ptr<float> m_q;
public:
    NDResourceClass():m_p(new int),m_q(new float){
        
    }
    
}
